package com.gitlab.amirmehdi.telegramwrapper;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.apache.logging.log4j.LogManager.getLogger;

@Service
public class TelegramService {
    private final List<RestTemplateTuple> restTemplates = new ArrayList<>();
    @Value("${application.telegram.token}")
    private String apiToken;


    private static final Logger log = getLogger();

    @PostConstruct
    public void fillRestTemplates() {
        try {
            FileUtils.readLines(new File("proxy-setting/valid_proxies.txt"), "UTF-8")
                    .forEach(s -> {
                        RestTemplate restTemplate = getRestTemplate(s);
                        restTemplates.add(new RestTemplateTuple(restTemplate));
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ResponseEntity<String> sendMessage(String chatId, String text) {
        if (restTemplates.isEmpty()) {
            log.error("have no proxy");
            return null;
        }
        return sendMessage(chatId, text, 3);
    }

    private ResponseEntity<String> sendMessage(String chatId, String text, int attemptNumber) {
        if (attemptNumber <= 0) {
            log.error("attempt number reached, sending to telegram has a problem");
            return null;
        }
        RestTemplateTuple tuple = restTemplates.get(ThreadLocalRandom.current().nextInt(restTemplates.size()));
        try {
            ResponseEntity<String> responseEntity =  requestToTelegram(tuple.getRestTemplate(), chatId, text);
            tuple.setErrorCount(0);
            return responseEntity;
        } catch (Exception e) {
            tuple.setErrorCount(tuple.getErrorCount() + 1);
            if (tuple.getErrorCount() > 5) {
                restTemplates.remove(tuple);
            }
            return sendMessage(chatId, text, attemptNumber - 1);
        }
    }

    public void refreshValidProxies() {
        try {
            List<String> validProxies = new ArrayList<>();
            FileUtils.readLines(new File("proxy-setting/http_proxies.txt"), "UTF-8")
                    .forEach(s -> {
                        try {
                            RestTemplate restTemplate = getRestTemplate(s);
                            //RestTemplate restTemplate = new RestTemplateBuilder(new ProxyCustomizer(hostName, port)).build();
                            requestToTelegram(restTemplate, "-1001314011261", String.format("%s is valid", s));
                            log.info("yes boy! valid proxy {}", s);
                            validProxies.add(s);
                        } catch (Exception e) {
                            log.error("invalid proxy {}", s);
                        }
                    });
            FileUtils.writeLines(new File("proxy-setting/valid_proxies.txt"), validProxies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private RestTemplate getRestTemplate(String s) {
        String hostName = s.split(":")[0];
        int port = Integer.parseInt(s.split(":")[1]);
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(6000);
        requestFactory.setReadTimeout(6000);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostName, port));
        requestFactory.setProxy(proxy);
        return new RestTemplate(requestFactory);
    }

    private ResponseEntity<String> requestToTelegram(RestTemplate restTemplate, String chatId, String text) {
        String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id={chatId}&text={text}&parse_mode=HTML";
        String url = String.format(urlString, apiToken);
        ResponseEntity<String> res = restTemplate.getForEntity(url, String.class, chatId, text);
        log.debug("url : {} , res : {} ", url, res.toString());
        return res;
    }
}
