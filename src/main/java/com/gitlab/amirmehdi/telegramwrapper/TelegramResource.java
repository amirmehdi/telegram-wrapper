package com.gitlab.amirmehdi.telegramwrapper;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TelegramResource {
    private final TelegramService telegramService;

    public TelegramResource(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    @GetMapping(value = "tg-send")
    public ResponseEntity<String> sendToTelegram(
            @RequestParam(value = "text") String text
            , @RequestParam(value = "chatId") String chatId) {
        return telegramService.sendMessage(chatId, text);
    }
}
