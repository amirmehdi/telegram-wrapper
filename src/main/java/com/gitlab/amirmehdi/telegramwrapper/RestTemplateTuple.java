package com.gitlab.amirmehdi.telegramwrapper;

import org.springframework.web.client.RestTemplate;

public class RestTemplateTuple {
    private RestTemplate restTemplate;
    private int errorCount;

    public RestTemplateTuple(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(int errorCount) {
        this.errorCount = errorCount;
    }
}
