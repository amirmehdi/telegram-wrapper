package com.gitlab.amirmehdi.telegramwrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegramWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramWrapperApplication.class, args);
    }

}
